<?php

namespace App\Entity;

use Symfony\Component\Uid\Uuid;

final class Category
{
   public Uuid $id;
   public string $name;
}