<?php

namespace App\Entity;

use Symfony\Component\Uid\Uuid;

final class Book
{
    public Uuid $id;
    public string  $title;
    public string $isbn;
    public  int $pageCount;
    public  string $publishedDate;
    public string $thumbnailUrl;
    public string $shortDescription;
    public string $longDescription;
    public string $status;
    public string $authors;
    public string $categories;
}