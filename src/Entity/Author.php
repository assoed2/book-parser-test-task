<?php

namespace App\Entity;

use Symfony\Component\Uid\Uuid;

final class Author
{
    public Uuid $id;
    public string $name;
}