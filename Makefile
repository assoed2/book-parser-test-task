# Variables
DOCKER_COMPOSE = docker-compose

# Create containers
up:
	$(DOCKER_COMPOSE) up -d

# Stop and remove containers
down:
	$(DOCKER_COMPOSE) down